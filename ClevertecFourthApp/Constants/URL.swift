//
//  URL.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 27.02.22.
//

import Foundation

enum URLs {
    static let baseURL: String = "https://belarusbank.by/open-banking/v1.0/atms"
}
