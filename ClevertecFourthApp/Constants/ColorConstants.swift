//
//  ColorConstants.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 22.02.22.
//

import UIKit

enum ColorConstants {
    static let background = UIColor.systemTeal
    static let collectionViewFontColor: UIColor = .black
    static let collectionViewBackgroundColorColor: UIColor = .white
}
