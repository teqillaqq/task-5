//
//  ContentConstants.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 22.02.22.
//

import Foundation

enum ContentConstants {
    static let segmentedControlFirstItem = "Карта"
    static let segmentedControlSecondItem = "Список банкоматов"
    static let reload = "Обновить"
    static let description = "Подробнее"
}
