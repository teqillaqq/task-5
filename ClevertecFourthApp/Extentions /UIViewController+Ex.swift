//
//  UIViewController+Ex.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 27.02.22.
//

import Foundation
import UIKit

extension UIViewController {

    func alertError(with error: Error) {
        let alert = UIAlertController(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        let reloadNetwork = UIAlertAction(title: "Нажмите кнопку обновить",
                                          style: .default) { _ in
        }
        let okAction = UIAlertAction(title: "Закрыть", style: .default)
        alert.addAction(reloadNetwork)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }

    func showSettingsAlert() {
        let alert = UIAlertController(title: nil,
                                      message: "This app requires access to gallery to proceed. Would you like to open settings and grant permission to gallery?",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { _ in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
        })
        present(alert, animated: true)
    }
}
