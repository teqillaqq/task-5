//
//  FullAtmInfoViewController.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 27.02.22.
//

import UIKit
import SnapKit
import MapKit

class FullAtmInfoViewController: UIViewController {

    var location: AtmAnnotations?
    var atmData: ATM?
    private let multiplier: Double = (4/5)

    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()

    private let contentView: UIView = {
        let contentView = UIView()
        contentView.translatesAutoresizingMaskIntoConstraints = false
        return contentView
    }()

    private let showRouteButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .darkGray
        button.setTitle("Построить маршрут", for: .normal)
        button.layer.cornerRadius = 18
        button.layer.borderWidth = 3
        button.layer.borderColor = UIColor.white.cgColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(showRoute), for: .touchUpInside)
        return button
    }()

    private let locationOfTheATMabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let addressLineLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let atmIdLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let availabilityLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let workingHoursLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let issuedCurrencyLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let cardsLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let currentStatusLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let servicesLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let coordinatesLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let contactDetailsLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupScrollView()
        setupViews()
        setupButton()
        guard let atmData = atmData else {
            return
        }
        configure(atm: atmData)
    }

    @objc private func showRoute(location: AtmAnnotations) {
        guard let atmData = atmData else {
            return
        }
        let title = atmData.address.addressLine
        let locationName = atmData.atmID
        let lat = Double(atmData.address.geolocation.geographicCoordinates.latitude)
        ?? 53.5439
        let long = Double(atmData.address.geolocation.geographicCoordinates.longitude)
        ?? 27.330
        let coordinate = CLLocationCoordinate2D(latitude: lat,
                                                longitude: long)
        let location = AtmAnnotations(title: title, locationName: locationName,
                                      coordinate: coordinate)
        let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }

    func configure(atm: ATM) {
        locationOfTheATMabel.text = atm.address.townName.isEmpty ?
        ("Адрес: " + atm.address.countrySubDivision.rawValue +
         " область, улица " + atm.address.streetName +
         ", дом " + atm.address.buildingNumber) :
        ("Адрес: " + atm.address.countrySubDivision.rawValue +
         " область, " + atm.address.townName +
         ", улица " + atm.address.streetName + ", дом " +
         atm.address.buildingNumber)

        addressLineLabel.text = "Место установки банкомата: " +
        atm.address.addressLine

        atmIdLabel.text = "ID номер банкомата: " +
        atm.atmID

        availabilityLabel.text = atm.availability.sameAsOrganization ?
        (atm.availability.access24Hours ?
         ("Доступ к банкомату круглосуточный и совпадает с режимом работы организации") :
            ("Доступ к банкомату не круглосуточный и совпадает с режимом работы организации")) :
        (atm.availability.access24Hours ?
         ("Доступ к банкомату круглосуточный, с режимом работы организации не совпадает") :
            ("Доступ к банкомату не круглосуточный, с режимом работы организации не совпадает"))

        if atm.availability.standardAvailability.day.isEmpty {
            workingHoursLabel.text = "На данный момент банкомат не работает"
        } else {
            var listOfDays = "Время работы:"
            var index = 0
            for day in atm.availability.standardAvailability.day {
                listOfDays.append(" \(day.dayCode.convertDayCodeToDay(dayCode: day.dayCode)) - с: " + atm.availability.standardAvailability.day[index].openingTime.rawValue +
                                  " до: " +
                                  atm.availability.standardAvailability.day[index].closingTime.rawValue +
                                  ", обед с: " +
                                  atm.availability.standardAvailability.day[index].dayBreak.breakFromTime.rawValue +
                                  " до: " +
                                  atm.availability.standardAvailability.day[index].dayBreak.breakToTime.rawValue + ";")
                if index >= atm.availability.standardAvailability.day.count { return }
                else {
                    index += 1
                }
            }
            workingHoursLabel.text = listOfDays
        }

        issuedCurrencyLabel.text = "Выдаваемая валюта: " + atm.currency.rawValue

        if atm.cards.isEmpty {
            cardsLabel.text = "На данный момент банкомат не принимает платежные карты"
        } else {
            var listOfCards = "Банкомат принимает следующие платежные карты:"
            for card in atm.cards {
                listOfCards.append(" " + card.rawValue + ",")
            }
            listOfCards.removeLast()
            cardsLabel.text = listOfCards
        }

        switch atm.currentStatus.rawValue {
        case "On":
            currentStatusLabel.text = "В данный момент банкомат работает"
        case "Off":
            currentStatusLabel.text = "В данный момент банкомат не работает"
        default:
            currentStatusLabel.text = "В данный момент нет данных, работает ли банкомат"
        }

        if atm.services.isEmpty {
            servicesLabel.text = "На данный момент нет информации об доступных услугах"
        } else {
            var listOfServices = "Банкомат предоставляет следующие услуги:"
            for service in atm.services {
                listOfServices.append(" " + service.serviceType.rawValue + ",")
            }
            listOfServices.removeLast()
            servicesLabel.text = listOfServices
        }

        if atm.services.isEmpty {
            descriptionLabel.text = "На данный момент нет дополнительной информации по услугам"
        } else {
            var listOfServices = "Дополнительная информация по услугам:"
            for service in atm.services {
                if service.serviceDescription.rawValue == "" {
                    print("Void serviceDescription rawValue")
                } else {
                    listOfServices.append(" " + service.serviceDescription.rawValue + ",")
                }
            }
            listOfServices.removeLast()
            descriptionLabel.text = listOfServices
        }

        coordinatesLabel.text = "Координаты банкомата: " +
        "широта - " + atm.address.geolocation.geographicCoordinates.latitude +
        ", долгота - " + atm.address.geolocation.geographicCoordinates.longitude

        if atm.contactDetails.phoneNumber == "" {
            contactDetailsLabel.text = "Контактный телефон не указан"
        } else {
            contactDetailsLabel.text = "Контактный телефон: " +
            atm.contactDetails.phoneNumber
        }
    }
}

extension FullAtmInfoViewController {

    private func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)

        scrollView.snp.makeConstraints { make in
            make.leading.top.trailing.equalTo(view.safeAreaLayoutGuide)
            make.bottom.equalToSuperview().offset(-125)
        }
        contentView.centerXAnchor.constraint(equalTo:
                                                scrollView.centerXAnchor).isActive = true
        contentView.widthAnchor.constraint(equalTo:
                                            scrollView.widthAnchor).isActive = true
        contentView.topAnchor.constraint(equalTo:
                                            scrollView.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo:
                                                scrollView.bottomAnchor).isActive = true
    }

    private func setupButton() {
        view.addSubview(showRouteButton)
        showRouteButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(35)
            make.trailing.bottom.equalTo(view.safeAreaLayoutGuide).offset(-35)
            make.height.equalTo(35)
        }
    }

    private func setupViews() {

        contentView.addSubview(locationOfTheATMabel)
        NSLayoutConstraint.activate([
            locationOfTheATMabel.centerXAnchor.constraint(equalTo:
                                                            contentView.centerXAnchor),
            locationOfTheATMabel.topAnchor.constraint(equalTo:
                                                        contentView.topAnchor),
            locationOfTheATMabel.widthAnchor.constraint(equalTo:
                                                            contentView.widthAnchor,
                                                        multiplier: multiplier)
        ])

        contentView.addSubview(addressLineLabel)
        NSLayoutConstraint.activate([
            addressLineLabel.centerXAnchor.constraint(equalTo:
                                                        contentView.centerXAnchor),
            addressLineLabel.topAnchor.constraint(equalTo:
                                                    locationOfTheATMabel.bottomAnchor,
                                                  constant: 25),
            addressLineLabel.widthAnchor.constraint(equalTo:
                                                        contentView.widthAnchor,
                                                    multiplier: multiplier)
        ])

        contentView.addSubview(atmIdLabel)
        NSLayoutConstraint.activate([
            atmIdLabel.centerXAnchor.constraint(equalTo:
                                                    contentView.centerXAnchor),
            atmIdLabel.topAnchor.constraint(equalTo:
                                                addressLineLabel.bottomAnchor,
                                            constant: 25),
            atmIdLabel.widthAnchor.constraint(equalTo:
                                                contentView.widthAnchor,
                                              multiplier: multiplier)
        ])

        contentView.addSubview(availabilityLabel)
        NSLayoutConstraint.activate([
            availabilityLabel.centerXAnchor.constraint(equalTo:
                                                        contentView.centerXAnchor),
            availabilityLabel.topAnchor.constraint(equalTo:
                                                    atmIdLabel.bottomAnchor,
                                                   constant: 25),
            availabilityLabel.widthAnchor.constraint(equalTo:
                                                        contentView.widthAnchor,
                                                     multiplier: multiplier)
        ])

        contentView.addSubview(workingHoursLabel)
        NSLayoutConstraint.activate([
            workingHoursLabel.centerXAnchor.constraint(equalTo:
                                                        contentView.centerXAnchor),
            workingHoursLabel.topAnchor.constraint(equalTo:
                                                    availabilityLabel.bottomAnchor, constant: 25),
            workingHoursLabel.widthAnchor.constraint(equalTo:
                                                        contentView.widthAnchor,
                                                     multiplier: multiplier)
        ])

        contentView.addSubview(issuedCurrencyLabel)
        NSLayoutConstraint.activate([
            issuedCurrencyLabel.centerXAnchor.constraint(equalTo:
                                                            contentView.centerXAnchor),
            issuedCurrencyLabel.topAnchor.constraint(equalTo:
                                                        workingHoursLabel.bottomAnchor, constant: 25),
            issuedCurrencyLabel.widthAnchor.constraint(equalTo:
                                                        contentView.widthAnchor, multiplier: multiplier)
        ])

        contentView.addSubview(cardsLabel)
        NSLayoutConstraint.activate([
            cardsLabel.centerXAnchor.constraint(equalTo:
                                                    contentView.centerXAnchor),
            cardsLabel.topAnchor.constraint(equalTo:
                                                issuedCurrencyLabel.bottomAnchor,
                                            constant: 25),
            cardsLabel.widthAnchor.constraint(equalTo:
                                                contentView.widthAnchor,
                                              multiplier: multiplier)
        ])

        contentView.addSubview(currentStatusLabel)
        NSLayoutConstraint.activate([
            currentStatusLabel.centerXAnchor.constraint(equalTo:
                                                            contentView.centerXAnchor),
            currentStatusLabel.topAnchor.constraint(equalTo:
                                                        cardsLabel.bottomAnchor,
                                                    constant: 25),
            currentStatusLabel.widthAnchor.constraint(equalTo:
                                                        contentView.widthAnchor,
                                                      multiplier: multiplier)
        ])

        contentView.addSubview(servicesLabel)
        NSLayoutConstraint.activate([
            servicesLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            servicesLabel.topAnchor.constraint(equalTo:
                                                currentStatusLabel.bottomAnchor,
                                               constant: 25),
            servicesLabel.widthAnchor.constraint(equalTo:
                                                    contentView.widthAnchor,
                                                 multiplier: multiplier)
        ])

        contentView.addSubview(descriptionLabel)
        NSLayoutConstraint.activate([
            descriptionLabel.centerXAnchor.constraint(equalTo:
                                                        contentView.centerXAnchor),
            descriptionLabel.topAnchor.constraint(equalTo:
                                                    servicesLabel.bottomAnchor,
                                                  constant: 25),
            descriptionLabel.widthAnchor.constraint(equalTo:
                                                        contentView.widthAnchor,
                                                    multiplier: multiplier)
        ])

        contentView.addSubview(coordinatesLabel)
        NSLayoutConstraint.activate([
            coordinatesLabel.centerXAnchor.constraint(equalTo:
                                                        contentView.centerXAnchor),
            coordinatesLabel.topAnchor.constraint(equalTo:
                                                    descriptionLabel.bottomAnchor,
                                                  constant: 25),
            coordinatesLabel.widthAnchor.constraint(equalTo:
                                                        contentView.widthAnchor,
                                                    multiplier: multiplier)
        ])

        contentView.addSubview(contactDetailsLabel)
        NSLayoutConstraint.activate([
            contactDetailsLabel.centerXAnchor.constraint(equalTo:
                                                            contentView.centerXAnchor),
            contactDetailsLabel.topAnchor.constraint(equalTo:
                                                        coordinatesLabel.bottomAnchor,
                                                     constant: 25),
            contactDetailsLabel.widthAnchor.constraint(equalTo:
                                                        contentView.widthAnchor,
                                                       multiplier: multiplier),
            contactDetailsLabel.bottomAnchor.constraint(equalTo:
                                                            contentView.bottomAnchor)
        ])
    }
}
