//
//  AtmPopUpView.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 24.02.22.
//


import Foundation
import UIKit
import MapKit

class PopUpWindow: UIViewController {

    private let popUpWindowView = PopUpWindowView()
    let gesture = UITapGestureRecognizer(target: self, action: #selector(clickAction))
    var InfoViewController: ATM?

    init(title: String, atm: ATM) {
        super.init(nibName: nil, bundle: nil)
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .overFullScreen
        InfoViewController = atm
        popUpWindowView.popupTitle.text = title
        popUpWindowView.configure(atm: atm)
        popUpWindowView.popupButton.addTarget(self, action: #selector(showInfoViewController), for: .touchUpInside)
        popUpWindowView.showRouteButton.addTarget(self, action: #selector(dismissPopView), for: .touchUpInside)
        popUpWindowView.addGestureRecognizer(gesture)

        view = popUpWindowView
    }
    @objc func clickAction() {
        self.dismiss(animated: true, completion: nil)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc private func showInfoViewController(){
        let fullAtmInfoViewController = FullAtmInfoViewController()
        guard let atm = InfoViewController else {
            return
        }
        fullAtmInfoViewController.atmData = atm
        self.present(fullAtmInfoViewController, animated: true, completion: nil)
    }

    @objc private func dismissPopView(){
        self.dismiss(animated: true, completion: nil)
    }
}

private class PopUpWindowView: UIView {

    private let borderWidth: CGFloat = 2.0
    private let popupView: UIView = {
        let popupView = UIView(frame: CGRect.zero)
        popupView.backgroundColor = .white
        popupView.layer.borderWidth = 2
        popupView.layer.masksToBounds = true
        popupView.layer.borderColor = UIColor.white.cgColor
        popupView.translatesAutoresizingMaskIntoConstraints = false
        return popupView
    }()

    lazy var popupTitle: UILabel = {
        let popupTitle = UILabel(frame: CGRect.zero)
        popupTitle.textColor = UIColor.white
        popupTitle.backgroundColor = ColorConstants.background
        popupTitle.layer.masksToBounds = true
        popupTitle.adjustsFontSizeToFitWidth = true
        popupTitle.clipsToBounds = true
        popupTitle.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        popupTitle.numberOfLines = 0
        popupTitle.textAlignment = .center
        popupTitle.translatesAutoresizingMaskIntoConstraints = false
        return popupTitle
    }()

    private let commonInformationLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let workingHoursLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    lazy var showRouteButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.setImage(UIImage(systemName: "xmark"), for: .normal)
        button.tintColor = ColorConstants.background
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    lazy var popupButton: UIButton = {
        let popupButton = UIButton(frame: CGRect.zero)
        popupButton.setTitleColor(UIColor.white, for: .normal)
        popupButton.setTitle(ContentConstants.description, for: .normal)
        popupButton.titleLabel?.font = UIFont.systemFont(ofSize: 18.0, weight: .bold)
        popupButton.backgroundColor = ColorConstants.background
        popupButton.translatesAutoresizingMaskIntoConstraints = false
        return popupButton
    }()

    init() {
        super.init(frame: CGRect.zero)
        backgroundColor = UIColor.black.withAlphaComponent(0.3)
        addSubview(popupView)

        popupView.addSubview(popupButton)
        NSLayoutConstraint.activate([
            popupView.widthAnchor.constraint(equalToConstant: 293),
            popupView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            popupView.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ])

        popupView.addSubview(popupTitle)
        NSLayoutConstraint.activate([
            popupTitle.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: borderWidth),
            popupTitle.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -52),
            popupTitle.topAnchor.constraint(equalTo: popupView.topAnchor, constant: borderWidth),
            popupTitle.heightAnchor.constraint(equalToConstant: 55)
        ])

        popupView.addSubview(showRouteButton)
        NSLayoutConstraint.activate([
            showRouteButton.leadingAnchor.constraint(equalTo: popupTitle.trailingAnchor),
            showRouteButton.topAnchor.constraint(equalTo: popupView.topAnchor),
            showRouteButton.trailingAnchor.constraint(equalTo: popupView.trailingAnchor),
            showRouteButton.bottomAnchor.constraint(equalTo: popupTitle.bottomAnchor)
        ])

        popupView.addSubview(commonInformationLabel)
        NSLayoutConstraint.activate([
            commonInformationLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 67),
            commonInformationLabel.topAnchor.constraint(equalTo: popupTitle.bottomAnchor, constant: 8),
            commonInformationLabel.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: 15),
            commonInformationLabel.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -15),
        ])

        popupView.addSubview(workingHoursLabel)
        NSLayoutConstraint.activate([
            workingHoursLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 67),
            workingHoursLabel.topAnchor.constraint(equalTo: commonInformationLabel.bottomAnchor, constant: 8),
            workingHoursLabel.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: 15),
            workingHoursLabel.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -15),
            workingHoursLabel.bottomAnchor.constraint(equalTo: popupButton.topAnchor, constant: -8)
        ])

        NSLayoutConstraint.activate([
            popupButton.heightAnchor.constraint(equalToConstant: 44),
            popupButton.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: borderWidth),
            popupButton.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -borderWidth),
            popupButton.bottomAnchor.constraint(equalTo: popupView.bottomAnchor, constant: -borderWidth)
        ])
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(atm: ATM) {
        var cashIn = ""
        for service in atm.services {
            cashIn.append(" " + service.serviceType.rawValue + ",")
        }
        if cashIn.contains("CashIn") {
            cashIn = " поддерживает"
        } else {
            cashIn = " не поддерживает"
        }

        commonInformationLabel.text = atm.address.townName.isEmpty ?
        ("Адрес: " + atm.address.countrySubDivision.rawValue +
         " область, улица " + atm.address.streetName +
         ", дом " + atm.address.buildingNumber) :
        ("Адрес: " + atm.address.countrySubDivision.rawValue +
         " область, " + atm.address.townName +
         ", улица " + atm.address.streetName + ", дом " + atm.address.buildingNumber +
         " || Выдаваемая валюта: " + atm.currency.rawValue +
         ". || CashIn" + cashIn)

        if atm.availability.standardAvailability.day.isEmpty {
            workingHoursLabel.text = "На данный момент банкомат не работает"
        } else {
            var listOfDays = "Время работы:"
            var index = 0
            for day in atm.availability.standardAvailability.day {
                listOfDays.append(" \(day.dayCode.convertDayCodeToDay(dayCode: day.dayCode)) - с: " + atm.availability.standardAvailability.day[index].openingTime.rawValue +
                                  " до: " +
                                  atm.availability.standardAvailability.day[index].closingTime.rawValue +
                                  ", обед с: " +
                                  atm.availability.standardAvailability.day[index].dayBreak.breakFromTime.rawValue +
                                  " до: " +
                                  atm.availability.standardAvailability.day[index].dayBreak.breakToTime.rawValue + ";")
                if index >= atm.availability.standardAvailability.day.count { return }
                else {
                    index += 1
                }
            }
            workingHoursLabel.text = listOfDays
        }
    }
}
