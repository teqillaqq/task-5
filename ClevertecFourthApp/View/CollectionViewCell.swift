//
//  CollectionViewCell.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 24.02.22.
//

import UIKit
import SnapKit

class CollectionViewCell: UICollectionViewCell {

    static let identifier = "CustomCollectionViewCell"

    private let locationOfTheATM: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = ColorConstants.collectionViewFontColor
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let workingHours: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = ColorConstants.collectionViewFontColor
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let issuedCurrency: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = ColorConstants.collectionViewFontColor
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var textLabelStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .equalSpacing
        stack.spacing = 3
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        initialize()
    }

    func configure(atm: ATM) {
        locationOfTheATM.text = atm.address.townName.isEmpty ? (atm.address.streetName + ", "
                                 + atm.address.buildingNumber) :
        (atm.address.townName + ", " + atm.address.streetName + ", " + atm.address.buildingNumber)
        workingHours.text = atm.availability.access24Hours ? ("Работает круглосуточно") :
        ("Работает с: " + atm.availability.standardAvailability.day[0].openingTime.rawValue +
         " до: " + atm.availability.standardAvailability.day[0].closingTime.rawValue) +
        ". Обед с: " + atm.availability.standardAvailability.day[0].dayBreak.breakFromTime.rawValue +
        " до: " + atm.availability.standardAvailability.day[0].dayBreak.breakToTime.rawValue
        issuedCurrency.text = ("Выдаваемая валюта: " + atm.currency.rawValue)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        locationOfTheATM.text = nil
        workingHours.text = nil
        issuedCurrency.text = nil
    }
}

extension CollectionViewCell {

    private func initialize() {
        contentView.backgroundColor = ColorConstants.collectionViewBackgroundColorColor
        contentView.layer.cornerRadius = 12
        contentView.clipsToBounds = true
        textLabelStack.addArrangedSubview(locationOfTheATM)
        textLabelStack.addArrangedSubview(workingHours)
        textLabelStack.addArrangedSubview(issuedCurrency)
        contentView.addSubview(textLabelStack)
        textLabelStack.snp.makeConstraints { make in
            make.leading.top.equalToSuperview().offset(5)
            make.trailing.bottom.equalToSuperview().offset(-5)
        }
    }
}

