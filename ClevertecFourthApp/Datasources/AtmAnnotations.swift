

import MapKit
import Contacts


class AtmAnnotations: NSObject, MKAnnotation {

    let title: String?
    let locationName: String?
    let coordinate: CLLocationCoordinate2D

    init(title: String, locationName: String?, coordinate: CLLocationCoordinate2D)
    {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate

        super.init()
    }

    var subtitle: String? {
        return locationName
    }

    class func from(atmData: ATM) -> AtmAnnotations?
    {
        let title = atmData.address.addressLine
        let locationName = atmData.atmID
        let lat = Double(atmData.address.geolocation.geographicCoordinates.latitude) ?? 53.5439
        let long = Double(atmData.address.geolocation.geographicCoordinates.longitude) ?? 27.330
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)

        return AtmAnnotations(title: title, locationName: locationName, coordinate: coordinate)
    }

    func mapItem() -> MKMapItem {
        let addressDictionary = [(String(CNPostalAddressStreetKey)) : subtitle]
        let placemark = MKPlacemark(coordinate: coordinate,
                                    addressDictionary: addressDictionary as [String : Any])
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "\(String(describing: title)) \(String(describing: subtitle))"
        return mapItem
    }
}

