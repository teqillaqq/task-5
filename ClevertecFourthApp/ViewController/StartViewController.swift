//
//  StartViewController.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 22.02.22.
//

import UIKit
import SnapKit
import MapKit
import CoreLocation
import Contacts

class StartViewController: UIViewController, MKMapViewDelegate {

    private let scrollView = UIScrollView()
    private let contentView = UIView()
    private let rangeInMeters: Double = 1700
    private let mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        return mapView
    }()
    private var collectionView: UICollectionView?
    private lazy var atmData = [ATM]() {
        didSet {
            DispatchQueue.main.async {
                for item in self.atmData {
                    if let atm = AtmAnnotations.from(atmData: item) {
                        self.annotationsArray.append(atm)
                    }
                }
            }
        }
    }
    private lazy var annotationsArray = [AtmAnnotations]() {
        didSet {
            mapView.addAnnotations(annotationsArray)
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    private lazy var atmDataByCity = Dictionary(grouping: atmData, by: { $0.address.townName })
    private lazy var objectArray = [GruppedByCytyObject]()
    private var previousLocation: CLLocation?
    lazy var locationManager: CLLocationManager = {
        var manager = CLLocationManager()
        manager.distanceFilter = 10
        manager.desiredAccuracy = kCLLocationAccuracyBest
        return manager
    }()

    private lazy var segmentedControl: UISegmentedControl = {
        segmentedControl = UISegmentedControl(items: [ContentConstants.segmentedControlFirstItem,
                                                      ContentConstants.segmentedControlSecondItem])
        segmentedControl.sizeToFit()
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(switchViewController(_ :)),
                                   for: .valueChanged)
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        return segmentedControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        initialize()

        NetworkWeatherManager.shared.fetchATMData { result in
            switch result {
            case .success(let ATMs):
                self.atmData = ATMs
            case .failure(let error):
                DispatchQueue.main.async {
                    self.alertError(with: error)
                }
            }
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: ContentConstants.reload,
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(reloadData))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationServices()
    }

    @objc private func reloadData() {
        navigationItem.rightBarButtonItem?.isEnabled = false
        NetworkWeatherManager.shared.fetchATMData { result in
            switch result {
            case .success(let ATMs):
                self.atmData = ATMs
            case .failure(let error):
                DispatchQueue.main.async {
                    self.alertError(with: error)
                }
            }
        }
    }

    @objc private func switchViewController(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            mapView.isHidden = false
            collectionView!.isHidden = true
        case 1:
            mapView.isHidden = true
            for (key, value) in atmDataByCity {
                objectArray.append(GruppedByCytyObject(sectionName: key,
                                           sectionObjects: value.sorted(by: { first, second in
                    return first.atmID < second.atmID
                })))
            }
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
            createCollection()
        default:
            print("Error: switchViewController index out of range")
        }
    }

    private func createCollection() {
        let layout = UICollectionViewFlowLayout ()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 50, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: (mapView.bounds.size.width * 0.3) - 4,
                                 height: (mapView.bounds.size.width * 0.65) - 4)

        collectionView = UICollectionView(frame: mapView.frame, collectionViewLayout: layout)
        guard let collectionView = collectionView else { return }
        collectionView.register(CollectionViewCell.self,
                                forCellWithReuseIdentifier: CollectionViewCell.identifier)
        collectionView.register(SectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: SectionHeader.reuserID)
        collectionView.dataSource = self
        collectionView.delegate = self
        view.addSubview(collectionView)
        collectionView.frame = mapView.frame
        collectionView.backgroundColor = ColorConstants.background

    }

    // MARK: - Location Services

    private func checkLocationServices() {
        guard CLLocationManager.locationServicesEnabled() else {
            showSettingsAlert()
            return
        }
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        checkAuthorizationForLocation()
    }

    private func checkAuthorizationForLocation() {
        switch locationManager.authorizationStatus {
        case .authorizedWhenInUse, .authorizedAlways:
            mapView.showsUserLocation = true
            centerViewOnUser()
            locationManager.startUpdatingLocation()
            previousLocation = getCenterLocation(for: mapView)
            break
        case .denied:
            showSettingsAlert()
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            showSettingsAlert()
            break
        @unknown default:
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            break
        }
    }

    private func centerViewOnUser() {
        guard let location = locationManager.location?.coordinate else { return }
        let coordinateRegion = MKCoordinateRegion.init(center: location,
                                                       latitudinalMeters: rangeInMeters,
                                                       longitudinalMeters: rangeInMeters)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func getCenterLocation(for mapView: MKMapView) -> CLLocation {
        let coordinates = mapView.centerCoordinate
        return CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
    }
}

extension StartViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {

        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
}

// MARK: - Show Popup

extension StartViewController {

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView){
        var popUpWindow: PopUpWindow!
        let location = view.annotation as? AtmAnnotations

        if let selectedAtm = atmData.first(where: { atm in
            atm.atmID == location?.locationName
        }) {
            popUpWindow = PopUpWindow(title: selectedAtm.address.addressLine, atm: selectedAtm)
            self.present(popUpWindow, animated: true, completion: nil)
        } else { return }
    }
}

// MARK: - Constraints for segmentedControl, mapView

extension StartViewController {

    private func initialize() {
        title = "Банкоматы"
        view.backgroundColor = ColorConstants.background

        view.addSubview(segmentedControl)
        segmentedControl.snp.makeConstraints { make in
            make.top.left.right.equalTo(view.safeAreaLayoutGuide)
            make.height.lessThanOrEqualTo(35)
        }

        view.addSubview(mapView)
        mapView.snp.makeConstraints { make in
            make.top.equalTo(segmentedControl.snp.bottom)
            make.left.right.equalTo(view.safeAreaLayoutGuide)
            make.bottom.equalTo(view)
        }
    }
}

// MARK: - CollectionView DataSource, Delegate

extension StartViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return objectArray.count
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objectArray[section].sectionObjects.count
    }

    func collectionView(_ tableView: UICollectionView, titleForHeaderInSection section: Int) -> String? {
        return objectArray[section].sectionName
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.identifier, for: indexPath) as! CollectionViewCell
        cell.configure(atm: objectArray[indexPath.section].sectionObjects[indexPath.row])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("User tapped on item \(indexPath.row)")
        let popUpWindow: PopUpWindow!
        popUpWindow = PopUpWindow(title: objectArray[indexPath.section].sectionObjects[indexPath.row].address.addressLine, atm: objectArray[indexPath.section].sectionObjects[indexPath.row])
        self.present(popUpWindow, animated: true, completion: nil)
        segmentedControl.selectedSegmentIndex = 0
        mapView.isHidden = false
        collectionView.isHidden = true
    }
}
