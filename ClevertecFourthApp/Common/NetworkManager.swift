//
//  StartViewController.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 22.02.22.
//

import Foundation
import MapKit

class NetworkWeatherManager {

    static let shared = NetworkWeatherManager()

    func fetchATMData(completionHandler: @escaping (Result<[ATM], Error>) -> Void) {
        guard let url = URL(string: URLs.baseURL) else { return }

        URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                completionHandler(.failure(error))
                return
            }

            guard let data = data else { return }
            do {
                let belarusbankDataATM = try JSONDecoder().decode(BelarusbankDataATM.self, from: data)
                let ATMs = belarusbankDataATM.data.atm

                completionHandler(.success(ATMs))
            } catch let error {
                completionHandler(.failure(error))
            }
        }.resume()
    }
}
